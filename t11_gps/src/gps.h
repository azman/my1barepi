/*----------------------------------------------------------------------------*/
#ifndef __MY1GPS_H__
#define __MY1GPS_H__
/*----------------------------------------------------------------------------*/
#include "uart.h"
/*----------------------------------------------------------------------------*/
#define BUFF_SIZE 128
/*----------------------------------------------------------------------------*/
#define NMEA_DELIM ",\r"
/*----------------------------------------------------------------------------*/
typedef struct _gps_data_t
{
	char *plat, *plon, *dutc, *tutc, *pvel, *pang;
	char *pchk, *p_ns, *p_ew, *psum, *buff, *pmag;
	char temp[BUFF_SIZE], nmea[BUFF_SIZE];
	float vlat, vlon;
	unsigned int test, stat;
	int size, lock;
}
gps_data_t;
/*----------------------------------------------------------------------------*/
#define GPS_STAT_ERROR (~(~0>>1))
#define GPS_STAT_ERROR_MISS (GPS_STAT_ERROR|0x01)
#define GPS_STAT_ERROR_OVERFLOW (GPS_STAT_ERROR|0x02)
#define GPS_STAT_ERROR_FORMAT (GPS_STAT_ERROR|0x04)
/*----------------------------------------------------------------------------*/
void gps_wait(gps_data_t* gpsd);
/*----------------------------------------------------------------------------*/
#endif /* __MY1GPS_H__ */
/*----------------------------------------------------------------------------*/
