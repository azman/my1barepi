/*----------------------------------------------------------------------------*/
#include "gps.h"
#include "utils.h"
#include "string.h"
/*----------------------------------------------------------------------------*/
void gps_wait(gps_data_t* gpsd)
{
	char *pbuf, *ptmp;
	gpsd->stat = 0;
	gpsd->size = 0;
	gpsd->lock = 0;
	while (1)
	{
		gpsd->test = uart_read();
		/* wait for init marker */
		if (!gpsd->size&&gpsd->test!='$') continue;
		if (!gpsd->test)
		{
			gpsd->temp[gpsd->size] = 0x0;
			gpsd->stat |= GPS_STAT_ERROR_MISS;
			return;
		}
		gpsd->temp[gpsd->size++] = (char) gpsd->test;
		if (gpsd->size==BUFF_SIZE)
		{
			gpsd->temp[BUFF_SIZE-1] = 0x0;
			gpsd->stat |= GPS_STAT_ERROR_OVERFLOW;
			return;
		}
		/* ensure GPRMC! */
		else if (gpsd->size==6)
		{
			if (strncmp(gpsd->temp,"$GPRMC",6))
			{
				gpsd->size = 0;
				continue;
			}
		}
		/* look for newline char */
		if (gpsd->test==(int)'\n')
		{
			gpsd->temp[gpsd->size-1] = 0x0;
			break;
		}
	}
	/* we chould get GPRMC line here! */
	strncpy(gpsd->nmea,gpsd->temp,BUFF_SIZE);
	pbuf = gpsd->nmea;
	strword(&pbuf,NMEA_DELIM); /* skip header! */
	gpsd->size = 1;
	gpsd->pchk = 0x0;
	while ((ptmp=strword(&pbuf,NMEA_DELIM)))
	{
		switch (gpsd->size)
		{
			case 1: gpsd->tutc = ptmp; break; /* utc time */
			case 2: gpsd->pchk = ptmp; break;
			case 3: gpsd->plat = ptmp; break; /* lat */
			case 4: gpsd->p_ns = ptmp; break;
			case 5: gpsd->plon = ptmp; break; /* lng */
			case 6: gpsd->p_ew = ptmp; break;
			case 7: gpsd->pvel = ptmp; break; /* speed over ground (knots) */
			case 8: gpsd->pang = ptmp; break; /* course over ground (degree) */
			case 9: gpsd->dutc = ptmp; break; /* utc date */
			/* 10:magnetic variation (degrees) */
			/* 11: */
			/* 12:checksum data begins with '*' */
		}
		gpsd->size++;
	}
	if (gpsd->size>=9&&gpsd->pchk&&gpsd->pchk[0]=='A'&&!gpsd->pchk[1])
	{
		gpsd->lock = 1;
		gpsd->vlat = str2float(gpsd->plat);
		gpsd->vlon = str2float(gpsd->plon);
	}
}
/*----------------------------------------------------------------------------*/
