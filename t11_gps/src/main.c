/*----------------------------------------------------------------------------*/
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "utils.h"
#include "string.h"
#include "gps.h"
#ifdef USE_OLED
#include "i2c.h"
#include "oled1306.h"
#else
#include "video.h"
#endif
/*----------------------------------------------------------------------------*/
#define GPS_GREET "MY1BAREPI GPS"
/*----------------------------------------------------------------------------*/
typedef struct _data_t
{
	char conv[32];
	gps_data_t gpsd;
#ifdef USE_OLED
	oled1306_t *oled;
#endif
}
data_t;
/*----------------------------------------------------------------------------*/
void show_gpsdata(data_t* data)
{
	if (data->gpsd.lock)
	{
#ifdef USE_OLED
		oled1306_cursor(data->oled,2,2);
		oled1306_text(data->oled,data->gpsd.pchk);
		oled1306_cursor(data->oled,3,0);
		oled1306_text(data->oled,"# ");
		oled1306_text(data->oled,data->gpsd.tutc);
		oled1306_cursor(data->oled,4,0);
		oled1306_text(data->oled,"# ");
		oled1306_text(data->oled,data->gpsd.plat);
		oled1306_text(data->oled,data->gpsd.p_ns);
		oled1306_cursor(data->oled,5,0);
		oled1306_text(data->oled,"# ");
		oled1306_text(data->oled,data->gpsd.plon);
		oled1306_text(data->oled,data->gpsd.p_ew);
		oled1306_cursor(data->oled,6,0);
		oled1306_text(data->oled,"# ");
		oled1306_text(data->oled,data->gpsd.dutc);
		oled1306_cursor(data->oled,7,0);
		oled1306_text(data->oled,"# ");
		float2str(data->conv,data->gpsd.vlat,4);
		oled1306_text(data->oled,data->conv);
/**
		float2str(data->conv,data->gpsd.vlon,4);
		oled1306_text(data->oled,data->conv);
*/
		oled1306_update(data->oled);
#else
		video_text_string("FOUND => ");
		video_text_string(data->gpsd.pchk);
		video_text_string("\n-- Chk: ");
		video_text_string(data->gpsd.dutc);
		video_text_string(" ");
		video_text_string(data->gpsd.tutc);
		video_text_string("\n-- Lat: ");
		video_text_string(data->gpsd.plat);
		video_text_string(data->gpsd.p_ns);
		video_text_string("\n-- Lng: ");
		video_text_string(data->gpsd.plon);
		video_text_string(data->gpsd.p_ew);
		video_text_string("\n\n");
#endif
	}
	else if (!data->gpsd.stat)
	{
#ifdef USE_OLED
		oled1306_cursor(data->oled,2,2);
		oled1306_text(data->oled,data->gpsd.pchk);
		oled1306_update(data->oled);
#else
		video_text_string("VOID!\n@@ Buff:{");
		video_text_string(data->gpsd.buff);
		video_text_string("}\n\n");
#endif
	}
	else
	{
		switch (data->gpsd.stat)
		{
		case GPS_STAT_ERROR_MISS:
#ifdef USE_OLED
			oled1306_cursor(&oled,2,2);
			oled1306_text(&oled,"? ");
			oled1306_update(&oled);
#else
			video_text_string("MISSED!\n** Buff:{");
			video_text_string(data->gpsd.buff);
			video_text_string(":");
			video_text_integer(data->gpsd.size);
			video_text_string("/");
			video_text_integer(BUFF_SIZE);
			video_text_string("}\n\n");
#endif
			break;
		case GPS_STAT_ERROR_OVERFLOW:
#ifdef USE_OLED
			oled1306_cursor(data->oled,2,2);
			oled1306_text(data->oled,"$ ");
			oled1306_update(data->oled);
#else
			video_text_string("OVERFLOW!\n** Buff:{");
			video_text_string(data->gpsd.buff);
			video_text_string(":");
			video_text_integer(data->gpsd.size);
			video_text_string("/");
			video_text_integer(BUFF_SIZE);
			video_text_string("}\n\n");
#endif
			break;
		default:
#ifdef USE_OLED
			oled1306_cursor(data->oled,2,2);
			oled1306_text(data->oled,"* ");
			oled1306_update(data->oled);
#else
			video_text_string("HUH?!\n@@ Buff:{");
			video_text_string(data->gpsd.buff);
			video_text_string("}\n\n");
#endif
		}
	}
}
/*----------------------------------------------------------------------------*/
void main(void)
{
#ifdef USE_OLED
	oled1306_t oled;
	char prompt[4];
#endif
	data_t data;
	/** initialize stuffs */
	timer_init();
	/** initialize uart for 9600bps */
	uart_init(UART_BAUD_DEFAULT);
#ifdef USE_OLED
	/** initialize i2c */
	i2c_init(I2C_SDA1_GPIO,I2C_SCL1_GPIO);
	i2c_set_wait_time(1);
	i2c_set_free_time(3);
	/** initialize oled display */
	oled1306_init(&oled,SSD1306_ADDRESS,OLED_TYPE_128x64,
		find_font_oled(UUID_FONT_OLED_8x8));
	oled1306_clear(&oled);
	oled1306_cursor(&oled,0,0);
	oled1306_text(&oled,GPS_GREET);
	oled1306_update(&oled);
	data.oled = &oled;
	prompt[0] = '>';
	prompt[1] = ' ';
	prompt[2] = 0x0;
#else
	/* hdmi display */
	video_init(VIDEO_RES_VGA);
	video_set_bgcolor(COLOR_BLUE);
	video_clear();
	video_text_cursor(1,0);
	video_text_string(GPS_GREET);
	video_text_cursor(3,0);
#endif
	/** do the thing... */
	while(1)
	{
#ifdef USE_OLED
		oled1306_cursor(&oled,2,0);
		if (prompt[0]=='>') prompt[0] = '|';
		else prompt[0] = '>';
		oled1306_text(&oled,prompt);
		oled1306_update(&oled);
#else
		video_text_string("Waiting... ");
#endif
		gps_wait(&data.gpsd);
		show_gpsdata(&data);
	}
}
/*----------------------------------------------------------------------------*/
