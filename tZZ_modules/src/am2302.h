/*----------------------------------------------------------------------------*/
#ifndef __MY1AM2302_H__
#define __MY1AM2302_H__
/*----------------------------------------------------------------------------*/
int am2302_read(int gpio_sens,unsigned int* ptemp,unsigned int* phumi);
/*----------------------------------------------------------------------------*/
#endif /* __MY1AM2302_H__ */
/*----------------------------------------------------------------------------*/
